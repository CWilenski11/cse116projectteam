/**
 * 
 */
package edu.buffalo.cse116;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.Test;

/**
 * -
 *
 */
public class SuggestionsTest {

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass

	public static void tearDownAfterClass() throws Exception {
	}

	// Suggestion would be answered by the next player because they have the
	// Player card;
	@Test
	public void gotPlayerCardTest() {
		Board b = new Board();
		ArrayList<Cards> p2 = new ArrayList<Cards>();
		Player tester1 = new Player();
		Player tester2 = new Player(p2);
		ArrayList<Cards> suggestions = new ArrayList<Cards>();
		Cards weapon = new Cards("Dagger");
		Cards room = new Cards("Kitchen");
		Cards culprit = new Cards("Professor Hertz");
		suggestions.add(weapon);
		suggestions.add(room);
		suggestions.add(culprit);
		Cards d = new Cards("Rope");
		Cards e = new Cards("DiningRoom");
		Cards f = new Cards("Professor Hertz");
		Cards g = new Cards("Professor Bina");
		Cards h = new Cards("Ballroom");
		Cards i = new Cards("Lead Pipe");
		p2.add(d);
		p2.add(e);
		p2.add(f);
		p2.add(g);
		p2.add(h);
		p2.add(i);
		ArrayList<Player> nextPlayers = new ArrayList<Player>();
		nextPlayers.add(tester2);
		assertEquals("player2 has PlayerCard!", tester1.compareToSuggestion(suggestions, nextPlayers), null);

	}

	// Suggestion would be answered by the next player because they have the
	// Room card;
	@Test
	public void gotPlayerRoomTest() {
		Board b = new Board();
		ArrayList<Cards> p2 = new ArrayList<Cards>();
		Player tester1 = new Player();
		Player tester2 = new Player(p2);
		ArrayList<Cards> suggestions = new ArrayList<Cards>();
		Cards weapon = new Cards("Dagger");
		Cards room = new Cards("Kitchen");
		Cards culprit = new Cards("Professor Hertz");
		suggestions.add(weapon);
		suggestions.add(room);
		suggestions.add(culprit);
		Cards d = new Cards("Rope");
		Cards e = new Cards("DiningRoom");
		Cards f = new Cards("Professor Alphonce");
		Cards g = new Cards("Professor Bina");
		Cards h = new Cards("Kitchen");
		Cards i = new Cards("Lead Pipe");
		p2.add(d);
		p2.add(e);
		p2.add(f);
		p2.add(g);
		p2.add(h);
		p2.add(i);
		ArrayList<Player> nextPlayers = new ArrayList<Player>();
		nextPlayers.add(tester2);
		assertEquals("player2 has RoomCard!", tester1.compareToSuggestion(suggestions, nextPlayers), null);

	}

	// Suggestion would be answered by the next player because they have the
	// Weapon card;
	@Test
	public void gotWeaponCardTest() {
		Board b = new Board();
		ArrayList<Cards> p2 = new ArrayList<Cards>();
		Player tester1 = new Player();
		Player tester2 = new Player(p2);
		ArrayList<Cards> suggestions = new ArrayList<Cards>();
		Cards weapon = new Cards("Dagger");
		Cards room = new Cards("Kitchen");
		Cards culprit = new Cards("Professor Hertz");
		suggestions.add(weapon);
		suggestions.add(room);
		suggestions.add(culprit);
		Cards d = new Cards("Dagger");
		Cards e = new Cards("DiningRoom");
		Cards f = new Cards("Professor Alphonce");
		Cards g = new Cards("Professor Bina");
		Cards h = new Cards("Ballroom");
		Cards i = new Cards("Lead Pipe");
		p2.add(d);
		p2.add(e);
		p2.add(f);
		p2.add(g);
		p2.add(h);
		p2.add(i);
		ArrayList<Player> nextPlayers = new ArrayList<Player>();
		nextPlayers.add(tester2);
		assertEquals("player2 has Weapon Card!", tester1.compareToSuggestion(suggestions, nextPlayers), null);
	}

	// Suggestion would be answered by the player after the next player because
	// they have 1 or more matching cards;
	@Test
	public void matchingTwoCards() {
		Board b = new Board();
		ArrayList<Cards> p2 = new ArrayList<Cards>();
		Player tester1 = new Player();
		Player tester2 = new Player(p2);
		ArrayList<Cards> suggestions = new ArrayList<Cards>();
		Cards weapon = new Cards("Dagger");
		Cards room = new Cards("Kitchen");
		Cards culprit = new Cards("Professor Hertz");
		suggestions.add(weapon);
		suggestions.add(room);
		suggestions.add(culprit);
		Cards d = new Cards("Dagger");
		Cards e = new Cards("DiningRoom");
		Cards f = new Cards("Professor Alphonce");
		Cards g = new Cards("Professor Hertz");
		Cards h = new Cards("Ballroom");
		Cards i = new Cards("Lead Pipe");
		p2.add(d);
		p2.add(e);
		p2.add(f);
		p2.add(g);
		p2.add(h);
		p2.add(i);
		ArrayList<Player> nextPlayers = new ArrayList<Player>();
		nextPlayers.add(tester2);
		assertEquals("player2 has Weapon Card!", tester1.compareToSuggestion(suggestions, nextPlayers), null);
	}

	// Suggestion would be answered by the player after the next player because
	// they have 1 or more matching cards;
	@Test
	public void thirdPlayerMatchingCards() {

		Board b = new Board();
		ArrayList<Cards> p2 = new ArrayList<Cards>();
		ArrayList<Cards> p3 = new ArrayList<Cards>();
		Player tester1 = new Player();
		Player tester2 = new Player(p2);
		Player tester3 = new Player(p3);
		ArrayList<Cards> suggestions = new ArrayList<Cards>();
		Cards weapon = new Cards("Dagger");
		Cards room = new Cards("Kitchen");
		Cards culprit = new Cards("Professor Hertz");
		suggestions.add(weapon);
		suggestions.add(room);
		suggestions.add(culprit);
		Cards d = new Cards("Rope");
		Cards e = new Cards("DiningRoom");
		Cards f = new Cards("Professor Alphonce");
		Cards g = new Cards("Professor Bina");
		Cards h = new Cards("Ballroom");
		Cards i = new Cards("Lead Pipe");
		p2.add(d);
		p2.add(e);
		p2.add(f);
		p2.add(g);
		p2.add(h);
		p2.add(i);
		Cards j = new Cards("Dagger");
		Cards k = new Cards("DiningRoom");
		Cards l = new Cards("Professor Alphonce");
		Cards m = new Cards("Professor Bina");
		Cards n = new Cards("Ballroom");
		Cards o = new Cards("Lead Pipe");
		p3.add(j);
		p3.add(k);
		p3.add(l);
		p3.add(m);
		p3.add(n);
		p3.add(o);
		ArrayList<Player> nextPlayers = new ArrayList<Player>();
		nextPlayers.add(tester2);
		nextPlayers.add(tester3);
		assertEquals("player3 has a similar card!", tester1.compareToSuggestion(suggestions, nextPlayers), null);
	}


	// Suggestion would be answered by the player immediately before player
	// making suggestion
	// because they have 1 or more matching cards;
	@Test
	public void playerBeforeSuggesterTest() {
		Board b = new Board();
		ArrayList<Cards> p1 = new ArrayList<Cards>();
		ArrayList<Cards> p3 = new ArrayList<Cards>();
		Player tester1 = new Player(p1);
		Player tester2 = new Player();
		Player tester3 = new Player(p3);
		ArrayList<Cards> suggestions = new ArrayList<Cards>();
		Cards weapon = new Cards("Dagger");
		Cards room = new Cards("Kitchen");
		Cards culprit = new Cards("Professor Hertz");
		suggestions.add(weapon);
		suggestions.add(room);
		suggestions.add(culprit);
		Cards d = new Cards("Rope");
		Cards e = new Cards("DiningRoom");
		Cards f = new Cards("Professor Alphonce");
		Cards g = new Cards("Professor Bina");
		Cards h = new Cards("Ballroom");
		Cards i = new Cards("Lead Pipe");
		p1.add(d);
		p1.add(e);
		p1.add(f);
		p1.add(g);
		p1.add(h);
		p1.add(i);
		Cards j = new Cards("Dagger");
		Cards k = new Cards("DiningRoom");
		Cards l = new Cards("Professor Alphonce");
		Cards m = new Cards("Professor Bina");
		Cards n = new Cards("Ballroom");
		Cards o = new Cards("Lead Pipe");
		p3.add(j);
		p3.add(k);
		p3.add(l);
		p3.add(m);
		p3.add(n);
		p3.add(o);
		ArrayList<Player> nextPlayers = new ArrayList<Player>();
		nextPlayers.add(tester1);
		nextPlayers.add(tester3);
		assertEquals("player1 has a similar card!", tester2.compareToSuggestion(suggestions, nextPlayers), null);

	}

	// Suggestion cannot be answered by any player but the player making the
	// suggestion has 1 or more matching cards
	@Test
	public void noMatchingCardsbutPlayerOne() {
		Board b = new Board();
		ArrayList<Cards> p1 = new ArrayList<Cards>();
		ArrayList<Cards> p2 = new ArrayList<Cards>();
		ArrayList<Cards> p3 = new ArrayList<Cards>();
		Player tester1 = new Player(p1);
		Player tester2 = new Player(p2);
		Player tester3 = new Player(p3);
		ArrayList<Cards> suggestions = new ArrayList<Cards>();
		Cards weapon = new Cards("Dagger");
		Cards room = new Cards("Kitchen");
		Cards culprit = new Cards("Professor Hertz");
		suggestions.add(weapon);
		suggestions.add(room);
		suggestions.add(culprit);
		Cards d = new Cards("Rope");
		Cards e = new Cards("DiningRoom");
		Cards f = new Cards("Professor Alphonce");
		Cards g = new Cards("Professor Bina");
		Cards h = new Cards("Ballroom");
		Cards i = new Cards("Lead Pipe");
		p2.add(d);
		p2.add(e);
		p2.add(f);
		p2.add(g);
		p2.add(h);
		p2.add(i);
		Cards j = new Cards("Rope");
		Cards k = new Cards("DiningRoom");
		Cards l = new Cards("Professor Alphonce");
		Cards m = new Cards("Professor Bina");
		Cards n = new Cards("Ballroom");
		Cards o = new Cards("Lead Pipe");
		p3.add(j);
		p3.add(k);
		p3.add(l);
		p3.add(m);
		p3.add(n);
		p3.add(o);
		p1.add(weapon);
		p1.add(d);
		p1.add(e);
		p1.add(f);
		p1.add(g);
		p1.add(h);

		ArrayList<Player> nextPlayers = new ArrayList<Player>();
		nextPlayers.add(tester2);
		nextPlayers.add(tester3);
		nextPlayers.add(tester1);
		assertEquals("player1 has a similar card!", tester1.compareToSuggestion(suggestions, nextPlayers), null);
	}

	// Suggestion cannot be answered by any player and the player making the
	// suggestion
	// does not have any matching cards
	@Test
	public void noMatchingCardsAtAll() {
		Board b = new Board();
		ArrayList<Cards> p1 = new ArrayList<Cards>();
		ArrayList<Cards> p2 = new ArrayList<Cards>();
		ArrayList<Cards> p3 = new ArrayList<Cards>();
		Player tester1 = new Player(p1);
		Player tester2 = new Player(p2);
		Player tester3 = new Player(p3);
		ArrayList<Cards> suggestions = new ArrayList<Cards>();
		Cards weapon = new Cards("Dagger");
		Cards room = new Cards("Kitchen");
		Cards culprit = new Cards("Professor Hertz");
		suggestions.add(weapon);
		suggestions.add(room);
		suggestions.add(culprit);
		Cards d = new Cards("Rope");
		Cards e = new Cards("DiningRoom");
		Cards f = new Cards("Professor Alphonce");
		Cards g = new Cards("Professor Bina");
		Cards h = new Cards("Ballroom");
		Cards i = new Cards("Lead Pipe");
		p2.add(d);
		p2.add(e);
		p2.add(f);
		p2.add(g);
		p2.add(h);
		p2.add(i);
		Cards j = new Cards("Rope");
		Cards k = new Cards("DiningRoom");
		Cards l = new Cards("Professor Alphonce");
		Cards m = new Cards("Professor Bina");
		Cards n = new Cards("Ballroom");
		Cards o = new Cards("Lead Pipe");
		p3.add(j);
		p3.add(k);
		p3.add(l);
		p3.add(m);
		p3.add(n);
		p3.add(o);
		p1.add(i);
		p1.add(d);
		p1.add(e);
		p1.add(f);
		p1.add(g);
		p1.add(h);

		ArrayList<Player> nextPlayers = new ArrayList<Player>();
		nextPlayers.add(tester2);
		nextPlayers.add(tester3);
		nextPlayers.add(tester1);
		assertEquals("no similar cards", tester1.compareToSuggestion(suggestions, nextPlayers), null);
	}
}
