package edu.buffalo.cse116;

import java.util.ArrayList;

import edu.buffalo.cse116.Cards;

public class Player {
	 int xlocation;
	 int ylocation;
	 Board b;
	ArrayList<Cards> Rc;
	String name;
	// Every time a player is created. He's created w/ a location and he gets a
	// set of cards
	public Player(){
		
	}
	public Player(ArrayList<Cards> c) {
		xlocation=0;
		ylocation=0;
		
		Rc=c;
	}

	
	public ArrayList<Cards> assignCards() {
		Cards c = new Cards();
		return c.generateHand();
	}
	// Method that sets a player name

	public void setLocation(int x, int y) {
		xlocation = x;
		ylocation = y;
	}

		
	public void setName(String n) {
		name = n;
	}

	public int getxLocation() {
		return xlocation;
	}

	public int getyLocation() {
		return ylocation;
	}
	
	public ArrayList<Cards> getCards(){
		return Rc;
		
	}

	// method that returns a player name
	public String getName() {

		return name;

	}

	public int MoveRight(int f) {
		
		xlocation += f;

		return xlocation;
	}

	
	public int MoveLeft( int f) {
		
		xlocation -= f;

		return xlocation;
	}

	public int MoveDown(int f) {
		
		ylocation += f;
		
		return ylocation;
	}

	public int MoveUp(int f) {
		
		ylocation -= f;

		return ylocation;
	}
	public ArrayList<Cards> getHand(){
		return Rc;
	}
	
		public void whosTurnIsIt(){
			
		}

	public boolean spotFull(int x, int y){
		
		if ((getxLocation()==x) && (getyLocation()==y)){
			return true;
		}
		return false;
	}
	 public ArrayList<Cards> enterDoor(Player p){
		 ArrayList<Cards> suggestions= new ArrayList<Cards>();
				if( b.IsDoor( p.getxLocation(), p.getyLocation()) == true){	
	 	//1st	// suggestions.add(player.getRoom); // need getRoom method. 
		//2nd	// suggestions.add( weapon ); // we will use a Jframe popup that will make you pick the other 2 suggestions
	 											// list every single card
	 	//3rd	// suggestions.add( suspect );
	    
				}
				return suggestions;
	 }
	public Player compareToSuggestion(ArrayList<Cards> suggestionList, ArrayList<Player> compareLists){

		  //For each card in first list
		  for(Cards first: suggestionList){

		      //For each list you wish to compare against
		      for(Player nextPlayer: compareLists){

		          //For each card in the list compare against first list
		          for(Cards next: nextPlayer.getHand()){
		              if(first.equals(next)){
		          return nextPlayer;
		          }
		              }
		          }
		      }
		      
		return null;
		
		  }
	
}

