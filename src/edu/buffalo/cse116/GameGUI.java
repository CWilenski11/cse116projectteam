package edu.buffalo.cse116;

import java.awt.Button;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class GameGUI {
	private JPanel panel2;
	JPanel panel3;
	private JLabel turn;
	private JLabel _move;
	JLabel card1;
	JLabel card2;
	JLabel card3;
	private Board Board;
	private JButton _d;
	private JButton b1;
	private JButton e1;
	private JFrame frame;
	private Dice die = new Dice();
	private Cards c = new Cards();
	ArrayList<Cards> p1hand;
	ArrayList<Cards> p2hand;
	ArrayList<Cards> p3hand;
	ArrayList<Cards> p4hand;
	ArrayList<Cards> p5hand;
	ArrayList<Cards> p6hand;
	private int dieroll;
	private JButton buttonOK = new JButton("OK");
	private JButton buttonOK1 = new JButton("OK");
	private ArrayList<Cards> _deck;
	ArrayList <String> suggestions = new ArrayList<String>();
	ArrayList <String> accusations = new ArrayList<String>();
	
	public GameGUI(Board board) {
		Board = board;
		frame = new JFrame("Clue Game");
		c.CreateDeck();
		p1hand = c.generateHand();
		p2hand = c.generateHand();
		p3hand = c.generateHand();
		p4hand = c.generateHand();
		p5hand = c.generateHand();
		p6hand = c.removeFinalThree();
		card1 = new JLabel();
		card2 = new JLabel();
		card3 = new JLabel();
		card1.setText(c.generateHandString(p1hand).get(0));
		card2.setText(c.generateHandString(p1hand).get(1));
		card3.setText(c.generateHandString(p1hand).get(2));

		panel2 = new JPanel();
		panel2.setLayout(new GridLayout(25, 25));

		frame.setSize(50, 50);

		JPanel panel_1 = new JPanel();

		panel_1.setLayout(new GridLayout(1, 0));

		turn = new JLabel();
		turn.setText("" +Board.getCurrentPlayer().getName()+ "'s turn");
		panel_1.add(turn);

	
		e1 = new JButton();
		e1.setText("Make Accusation");
		e1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
// accusation	
				
				Object[] options = {"Yes, please",
				                    "No, thanks" };
				
				int n = JOptionPane.showOptionDialog(frame,
				    "Would you like to make Accusation? ",
				    "Make Accusation?",
				    JOptionPane.YES_NO_OPTION,
				    JOptionPane.QUESTION_MESSAGE,
				    null,
				    options,
				    options[1]);
				
				
				// When yes is selected
				if (n==0){
					//compare players accusation with the cards in middle of deck
					
					// frame for Accusation
					
					final JFrame s_frame = new JFrame("Enter Accusation");
					s_frame.setVisible(true);
					s_frame.setSize(500,600);
				// panel for radio buttons. Takes input for weapon and character suggestion
				    JPanel s_panel = new JPanel();
					s_panel.setLayout(new BoxLayout(s_panel, BoxLayout.Y_AXIS));
					s_frame.add(s_panel);
					// weapon buttons w/ itemListeners.
						JRadioButton w1 = new JRadioButton("CandleStick");
						w1.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add accusation
						        	accusations.add(c.createCard("CandleStick").getType());
			
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton w2 = new JRadioButton("Dagger");
						w2.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Dagger").getType());

						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton w3 = new JRadioButton("Lead Pipe");
						w3.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Lead Pipe").getType());

						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton w4 = new JRadioButton("Revolver");
						w4.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Revolver").getType());

						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton w5 = new JRadioButton("Rope");
						w5.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Rope").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton w6 = new JRadioButton("Spanner");
						w6.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Spanner").getType());
					
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
					// Button Group allows only 1 chosen radiobutton
						ButtonGroup weapons = new ButtonGroup();
						weapons.add(w1);
						weapons.add(w2);
						weapons.add(w3);
						weapons.add(w4);
						weapons.add(w5);
						weapons.add(w6);
						
					// character buttons w/ itemListeners
						JRadioButton c1 = new JRadioButton("Miss Scarlet");
						c1.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Miss Scarlet").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton c2 = new JRadioButton("Professor Plum");
						c2.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Professor Plum").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton c3 = new JRadioButton("Mr.Green");
						c3.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Mr.Green").getType());
						        
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton c4 = new JRadioButton("Mrs.White");
						c4.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Mrs.White").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton c5= new JRadioButton("Mrs.Peacock");
						c5.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Mrs.Peacock").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton c6 = new JRadioButton("Col.Mustard");
						c6.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Col.Mustard").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						ButtonGroup characters = new ButtonGroup();
						characters.add(c1);
						characters.add(c2);
						characters.add(c3);
						characters.add(c4);
						characters.add(c5);
						characters.add(c6);
						// rooms buttons w/ itemListeners
						JRadioButton r1 = new JRadioButton("Kitchen");
						c1.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Kitchen").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton r2 = new JRadioButton("Ballroom");
						c2.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Ballroom").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton r3 = new JRadioButton("Dining Room");
						c3.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Dining Room").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton r4 = new JRadioButton("Lounge");
						c4.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Lounge").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton r5= new JRadioButton("Game Room");
						c5.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Game Room").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton r6 = new JRadioButton("Library");
						c6.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Library").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton r7 = new JRadioButton("Indoor Pool");
						c4.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Indoor Pool").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton r8= new JRadioButton("Bedroom");
						c5.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Bedroom").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton r9 = new JRadioButton("Guest Room");
						c6.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	accusations.add(c.createCard("Guest Room").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						
						ButtonGroup rooms = new ButtonGroup();
						rooms.add(r1);
						rooms.add(r2);
						rooms.add(r3);
						rooms.add(r4);
						rooms.add(r5);
						rooms.add(r6);
						rooms.add(r7);
						rooms.add(r8);
						rooms.add(r9);
						
						
						// Add buttons to accusation panel
						s_panel.add(r1);
						s_panel.add(r2);
						s_panel.add(r3);
						s_panel.add(r4);
						s_panel.add(r5);
						s_panel.add(r6);
						s_panel.add(r7);
						s_panel.add(r8);
						s_panel.add(r9);
						s_panel.add(w1);
						s_panel.add(w2);
						s_panel.add(w3);
						s_panel.add(w4);
						s_panel.add(w5);
						s_panel.add(w6);
						s_panel.add(c1);
						s_panel.add(c2);
						s_panel.add(c3);
						s_panel.add(c4);
						s_panel.add(c5);
						s_panel.add(c6);
					// if the player is correct they win etc
					  // accusation
					  // envelope cards
					 // we know that both ArrayLists will have the same amount of strings in them so this should work.
						s_panel.add(buttonOK1);
						buttonOK1.addActionListener(new ActionListener() {
					    	public void actionPerformed(ActionEvent e){
					 if(accusations.containsAll(c.generateHandString(c._envelope)) && c.generateHandString(c._envelope).containsAll(accusations)){ 
					 	//JFrame winfr = new JFrame("Winner!");
					  	//winfr.setVisible(true);
					  	//winfr.setSize(200,200);
					    //JLabel winner = new JLabel();
					    //winner.setText("Congratulations you won!");
					    
					    JOptionPane.showMessageDialog(frame,
					    	    "Eggs are not supposed to be green but you WONNNNNNNN",
					    	    "Congratulations",
					    	    JOptionPane.INFORMATION_MESSAGE
					    	    );
					    
					    //winfr.add(winner);
					   } else {
						   JFrame winfr = new JFrame("You Lost!");
						  	winfr.setVisible(true);
						  	winfr.setSize(200,200);
						    JLabel loser = new JLabel();
						    loser.setText("You Lost!");
						    winfr.add(loser);
						    Board.Playerlist.remove(Board.Playerlist.get(Board.Playerlist.indexOf(Board.getCurrentPlayer())));
					   
					   }
					
					    	}
				});
				}
					 
					//if the player is wrong they no longer have a turn!
				
				
				// when no is selected
				if (n==1){
					//dont need to do  anything it cancels
					
				}
				
			
			}
			
			});
			panel_1.add(e1);
				
				
			
		
		
		b1 = new JButton();
		b1.setText("End Turn");
		b1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

					
				Board.SwitchPlayers();
				turn.setText(Board.getCurrentPlayer().getName() + "'s turn");
				if(Board.getCurrentPlayer().getName().equals("Miss Scarlet")){
					card1.setText(c.generateHandString(p1hand).get(0));
					card2.setText(c.generateHandString(p1hand).get(1));
					card3.setText(c.generateHandString(p1hand).get(2));
				}
				if(Board.getCurrentPlayer().getName().equals("Professor Plum")){
					card1.setText(c.generateHandString(p2hand).get(0));
					card2.setText(c.generateHandString(p2hand).get(1));
					card3.setText(c.generateHandString(p2hand).get(2));
				}
				if(Board.getCurrentPlayer().getName().equals("Mr.Green")){
					card1.setText(c.generateHandString(p3hand).get(0));
					card2.setText(c.generateHandString(p3hand).get(1));
					card3.setText(c.generateHandString(p3hand).get(2));
				}
				if(Board.getCurrentPlayer().getName().equals("Mrs.White")){
					card1.setText(c.generateHandString(p4hand).get(0));
					card2.setText(c.generateHandString(p4hand).get(1));
					card3.setText(c.generateHandString(p4hand).get(2));
				}
				if(Board.getCurrentPlayer().getName().equals("Mrs.Peacock")){
					card1.setText(c.generateHandString(p5hand).get(0));
					card2.setText(c.generateHandString(p5hand).get(1));
					card3.setText(c.generateHandString(p5hand).get(2));
				}
				if(Board.getCurrentPlayer().getName().equals("Col.Mustard")){
					card1.setText(c.generateHandString(p6hand).get(0));
					card2.setText(c.generateHandString(p6hand).get(1));
					card3.setText(c.generateHandString(p6hand).get(2));
				}
			}

		});

		panel_1.add(b1);
		final JLabel _move = new JLabel();
		panel_1.add(_move);
		
		_d = new JButton();
		_d.setText("Roll Dice");
		panel_1.add(_d);
		_d.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				die.roll();
			    dieroll = die.getDieRoll();
				_d.setText("dice  roll :" + dieroll);
				_move.setText("Move: "+ dieroll);
			}
		});
		
	

		JButton arrowUp = new JButton("^");
		panel_1.add(arrowUp);
		arrowUp.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				JFrame frame2 = new JFrame("Moves Players can make");
			    frame2.setVisible(true);
			    frame2.setSize(200,200);
			    JButton up = new JButton("Move up");
			    frame2.add(up);
			    up.addActionListener(new ActionListener() {
			    	
			    	public void actionPerformed(ActionEvent e){
			    		if(Board.IsDoor(Board.getCurrentPlayer().getxLocation()-1,Board.getCurrentPlayer().getyLocation())){
			    			Board.MoveUp(Board.getCurrentPlayer());
			    			exampleMethod();
			    			throughDoor(Board.getCurrentPlayer().getxLocation()-1,Board.getCurrentPlayer().getyLocation());
			    			
			    		}
			    		else{
			    		Board.MoveUp(Board.getCurrentPlayer());
			    		exampleMethod();
			    		}
			    	}
			    });
			    // i was thinking maybe we add another action listener to up and all the other arrows that will move the player up-Abass
			    
			   
			}
		});
	
					
		JButton arrowDown = new JButton("v");
		panel_1.add(arrowDown);
		
		arrowDown.addActionListener(new ActionListener() {
			
public void actionPerformed(ActionEvent e) {
				
				JFrame frame2 = new JFrame("Moves Players can make");
			    frame2.setVisible(true);
			    frame2.setSize(200,200);
			    JButton Down = new JButton("Move Down");
			    frame2.add(Down);
			    Down.addActionListener(new ActionListener() {
			    	public void actionPerformed(ActionEvent e){
			    		if(Board.IsDoor(Board.getCurrentPlayer().getxLocation()+1,Board.getCurrentPlayer().getyLocation())){
			    			Board.MoveDown(Board.getCurrentPlayer());
			    			exampleMethod();
			    			throughDoor(Board.getCurrentPlayer().getxLocation()+1,Board.getCurrentPlayer().getyLocation());
			    			
			    		}
			    		else{
			    		Board.MoveDown(Board.getCurrentPlayer());
			    		exampleMethod();
			    		}
			    		
			    	}
			    });
			    // i was thinking maybe we add another action listener to down and all the other arrows that will move the player up-Abass
}
});  
			   
					
		
	
		JButton arrowLeft = new JButton("<");
		panel_1.add(arrowLeft);
		arrowLeft.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				JFrame frame2 = new JFrame("Moves Players can make");
			    frame2.setVisible(true);
			    frame2.setSize(200,200);
			    JButton left = new JButton("Move Left");
			    frame2.add(left);
			    left.addActionListener(new ActionListener() {
			    	public void actionPerformed(ActionEvent e){
			    		if(Board.IsDoor(Board.getCurrentPlayer().getxLocation(),Board.getCurrentPlayer().getyLocation()-1)){
			    			Board.MoveLeft(Board.getCurrentPlayer());
			    			exampleMethod();
			    			throughDoor(Board.getCurrentPlayer().getxLocation(),Board.getCurrentPlayer().getyLocation()-1);
			    			
			    		}
			    		else{
			    		Board.MoveLeft(Board.getCurrentPlayer());
			    		exampleMethod();
			    		}
			    	}
			    });
			    // i was thinking maybe we add another action listener to left and all the other arrows that will move the player up-Abass
			    
			   
			}
		});
	
		JButton arrowRight = new JButton(">");
		panel_1.add(arrowRight);
		arrowRight.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				JFrame frame2 = new JFrame("Moves Players can make");
			    frame2.setVisible(true);
			    frame2.setSize(200,200);
			    JButton right = new JButton("Move Right");
			    frame2.add(right);
			    right.addActionListener(new ActionListener() {
			    	public void actionPerformed(ActionEvent e){
			    		if(Board.IsDoor(Board.getCurrentPlayer().getxLocation(),Board.getCurrentPlayer().getyLocation()+1)){
			    			Board.MoveRight(Board.getCurrentPlayer());
			    			exampleMethod();
			    			throughDoor(Board.getCurrentPlayer().getxLocation(),Board.getCurrentPlayer().getyLocation()+1);
			    			
			    		}
			    		else{
			    		Board.MoveRight(Board.getCurrentPlayer());
			    		exampleMethod();
			    		}
			    	}
			    });
			    // i was thinking maybe we add another action listener to right and all the other arrows that will move the player up-Abass
			    
			   
			}
		});
		//  suggestions for weapon and character
		//
		//
	    panel3 = new JPanel();
		panel3.setLayout(new GridLayout(4,0));
		panel3.add(card1);
		panel3.add(card2);
		panel3.add(card3);
		
		exampleMethod();
		frame.add(panel2);
		frame.add(panel_1);
		frame.add(panel3);
		// changed from grid layout to box layout
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
		// frame.add(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// frame.getContentPane();
		frame.pack();
		frame.setVisible(true);
		
	}
	public void throughDoor(final int x, final int y){
		JFrame frame = new JFrame("Enter Room?");
		frame.setVisible(true);
		frame.setSize(200, 200);
		JButton b = new JButton();
		b.setText("Enter Room");
		frame.add(b);
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				JFrame frame1 = new JFrame("Make Suggestion");
				frame1.setVisible(true);
				frame1.setSize(200,200);
				frame1.setLayout(new GridLayout(2,0));
				JButton makeSuggestion = new JButton("Make Suggestion");
				frame1.add(makeSuggestion);
				makeSuggestion.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
					    
					// frame for suggestion
						final JFrame s_frame = new JFrame("Make a Suggestion");
						s_frame.setVisible(true);
						s_frame.setSize(500,600);
					// panel for radio buttons. Takes input for weapon and character suggestion
					    JPanel s_panel = new JPanel();
						s_panel.setLayout(new BoxLayout(s_panel, BoxLayout.Y_AXIS));
						s_frame.add(s_panel);

					// weapon buttons w/ itemListeners.
					// if button is picked, it will add it to the suggestions arraylist
						JRadioButton w1 = new JRadioButton("CandleStick");
						w1.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add suggestion(CandleStick card) to Arraylist<Cards> suggestions
						        	suggestions.add(c.createCard("CandleStick").getType());
						        	
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton w2 = new JRadioButton("Dagger");
						w2.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add suggestion to Arraylist<Cards> suggestions
						        	suggestions.add(c.createCard("Dagger").getType());
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton w3 = new JRadioButton("Lead Pipe");
						w3.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add suggestion to Arraylist<Cards> suggestions
						        	suggestions.add(c.createCard("Lead Pipe").getType());
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton w4 = new JRadioButton("Revolver");
						w4.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add suggestion to Arraylist<Cards> suggestions
						        	suggestions.add(c.createCard("Revolver").getType());
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton w5 = new JRadioButton("Rope");
						w5.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add suggestion to Arraylist<Cards> suggestions
						        	suggestions.add(c.createCard("Rope").getType());
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton w6 = new JRadioButton("Spanner");
						w6.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add suggestion to Arraylist<Cards> suggestions
						        	
						        	suggestions.add(c.createCard("Spanner").getType());
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
					// Button Group allows only 1 chosen radiobutton
						ButtonGroup weapons = new ButtonGroup();
						weapons.add(w1);
						weapons.add(w2);
						weapons.add(w3);
						weapons.add(w4);
						weapons.add(w5);
						weapons.add(w6);
					// character buttons w/ itemListeners
						JRadioButton c1 = new JRadioButton("Miss Scarlet");
						c1.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add suggestion to Arraylist<Cards> suggestions
						        	suggestions.add(c.createCard("Miss Scarlet").getType());
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton c2 = new JRadioButton("Professor Plum");
						c2.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add suggestion to Arraylist<Cards> suggestions
						        	suggestions.add(c.createCard("Professor Plum").getType());
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton c3 = new JRadioButton("Mr.Green");
						c3.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add suggestion to Arraylist<Cards> suggestions
						        	suggestions.add(c.createCard("Mr.Green").getType());
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton c4 = new JRadioButton("Mrs.White");
						c4.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add suggestion to Arraylist<Cards> suggestions
						        	suggestions.add(c.createCard("Mrs.White").getType());
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton c5= new JRadioButton("Mrs.Peacock");
						c5.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add suggestion to Arraylist<Cards> suggestions
						        	suggestions.add(c.createCard ("Mrs.Peacock").getType());
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						JRadioButton c6 = new JRadioButton("Col.Mustard");
						c6.addItemListener(new ItemListener() {
							public void itemStateChanged(ItemEvent event) {
								int state = event.getStateChange();
						        if (state == ItemEvent.SELECTED) {
						        	// add suggestion to Arraylist<Cards> suggestions
						        	suggestions.add(c.createCard("Col.Mustard").getType());
						        } else if (state == ItemEvent.DESELECTED) {
						            // do something else when the button is deselected
						        }
							}
						});
						//Room buttons
						if(Board.getGrid()[Board.getCurrentPlayer().getxLocation()][Board.getCurrentPlayer().getyLocation()-1] == 1){
							JRadioButton r1 = new JRadioButton("Kitchen",true);
							s_panel.add(r1);
							suggestions.add(c.createCard("Kitchen").getType());
						}
						if(Board.getGrid()[Board.getCurrentPlayer().getxLocation()][Board.getCurrentPlayer().getyLocation()-1] == 2){
						JRadioButton r2 = new JRadioButton("Ballroom",true);
						s_panel.add(r2);
						suggestions.add(c.createCard("Ballroom").getType());
						}
						if(Board.getGrid()[Board.getCurrentPlayer().getxLocation()+1][Board.getCurrentPlayer().getyLocation()] == 3){
						JRadioButton r3 = new JRadioButton("Dining Room",true);
						s_panel.add(r3);
						suggestions.add(c.createCard("Dining Room").getType());
						}
						if(Board.getGrid()[Board.getCurrentPlayer().getxLocation()][Board.getCurrentPlayer().getyLocation()-1] == 4){
						JRadioButton r4 = new JRadioButton("Lounge",true);
						s_panel.add(r4);
						suggestions.add(c.createCard("Lounge").getType());
						}
						if(Board.getGrid()[Board.getCurrentPlayer().getxLocation()+1][Board.getCurrentPlayer().getyLocation()] == 5){
						JRadioButton r5 = new JRadioButton("Game Room",true);
						s_panel.add(r5);
						suggestions.add(c.createCard("Game Room").getType());
						}
						if(Board.getGrid()[Board.getCurrentPlayer().getxLocation()+1][Board.getCurrentPlayer().getyLocation()] == 6){
						JRadioButton r6 = new JRadioButton("Library",true);
						s_panel.add(r6);
						suggestions.add(c.createCard("Library").getType());
						}
						if(Board.getGrid()[Board.getCurrentPlayer().getxLocation()][Board.getCurrentPlayer().getyLocation()+1] == 7){
						JRadioButton r7 = new JRadioButton("Indoor Pool",true);
						s_panel.add(r7);
						suggestions.add(c.createCard("Indoor Pool").getType());
						}
						if(Board.getGrid()[Board.getCurrentPlayer().getxLocation()-1][Board.getCurrentPlayer().getyLocation()] == 8){
						JRadioButton r8 = new JRadioButton("Bedroom",true);
						s_panel.add(r8);
						suggestions.add(c.createCard("Bedroom").getType());
						}
						if(Board.getGrid()[Board.getCurrentPlayer().getxLocation()-1][Board.getCurrentPlayer().getyLocation()] == 9){
						JRadioButton r9 = new JRadioButton("Guest Room",true);
						s_panel.add(r9);
						suggestions.add(c.createCard("Guest Room").getType());
						}
					// Button Group allows only 1 chosen radiobutton
						ButtonGroup characters = new ButtonGroup();
						characters.add(c1);
						characters.add(c2);
						characters.add(c3);
						characters.add(c4);
						characters.add(c5);
						characters.add(c6);
					// Add buttons to suggestionpanel
						s_panel.add(w1);
						s_panel.add(w2);
						s_panel.add(w3);
						s_panel.add(w4);
						s_panel.add(w5);
						s_panel.add(w6);
						s_panel.add(c1);
						s_panel.add(c2);
						s_panel.add(c3);
						s_panel.add(c4);
						s_panel.add(c5);
						s_panel.add(c6);
					// panel for "OK" button
						s_panel.add(buttonOK);
						buttonOK.addActionListener(new ActionListener() {
					    	public void actionPerformed(ActionEvent e){
					    		boolean found = false;
					    		ArrayList<String> p1 = new ArrayList<String>();
					    		p1.add(c.generateHandString(p1hand).get(0));
					    		p1.add(c.generateHandString(p1hand).get(1));
					    		p1.add(c.generateHandString(p1hand).get(2));
					    		ArrayList<String> p2 = new ArrayList<String>();
					    		p2.add(c.generateHandString(p1hand).get(0));
					    		p2.add(c.generateHandString(p1hand).get(1));
					    		p2.add(c.generateHandString(p1hand).get(2));
					    		ArrayList<String> p3 = new ArrayList<String>();
					    		p3.add(c.generateHandString(p1hand).get(0));
					    		p3.add(c.generateHandString(p1hand).get(1));
					    		p3.add(c.generateHandString(p1hand).get(2));
					    		ArrayList<String> p4 = new ArrayList<String>();
					    		p4.add(c.generateHandString(p1hand).get(0));
					    		p4.add(c.generateHandString(p1hand).get(1));
					    		p4.add(c.generateHandString(p1hand).get(2));
					    		ArrayList<String> p5 = new ArrayList<String>();
					    		p5.add(c.generateHandString(p1hand).get(0));
					    		p5.add(c.generateHandString(p1hand).get(1));
					    		p5.add(c.generateHandString(p1hand).get(2));
					    		ArrayList<String> p6 = new ArrayList<String>();
					    		p6.add(c.generateHandString(p1hand).get(0));
					    		p6.add(c.generateHandString(p1hand).get(1));
					    		p6.add(c.generateHandString(p1hand).get(2));
					    		for(String first: suggestions){
					    			for(String second: p1){
					    				if(first.equals(second)){
					    					System.out.println("Miss Scarlett:" + second.toString());
					    					found = true;
					    					break;
					    				}
					    			}
					    		}
					    		for(String first: suggestions){
					    			for(String second: p2){
					    				if(first.equals(second)){
					    					System.out.println("Professor Plum:" + second.toString());
					    					found = true;
					    					break;
					    				}
					    			}
					    		}
					    		for(String first: suggestions){
					    			for(String second: p3){
					    				if(first.equals(second)){
					    					System.out.println("Mr. Green:" + second.toString());
					    					found = true;
					    					break;
					    				}
					    			}
					    		}
					    		for(String first: suggestions){
					    			for(String second: p4){
					    				if(first.equals(second)){
					    					System.out.println("Mrs. White:" + second.toString());
					    					found = true;
					    					break;
					    				}
					    			}
					    		}
					    	
					    		for(String first: suggestions){
					    			for(String second: p5){
					    				if(first.equals(second)){
					    					System.out.println("Mrs. Peacock:" + second.toString());
					    					found = true;
					    					break;
					    				}
					    			}
					    		}
					    		for(String first: suggestions){
					    			for(String second: p6){
					    				if(first.equals(second)){
					    					System.out.println("Col. Mustard:" + second.toString());
					    					found = true;
					    					break;
					    				}
					    			}
					    		}
					    		if (!found){
					    			    System.out.println("Nobody can disprove the suggestion");
					    		}
					    	}

					    });

					}
					
				});

				if(Board.CanUseSecretPassage(x, y)){
					JButton b4 = new JButton("Use Secret Passage");
					frame1.add(b4);
					b4.addActionListener(new ActionListener() {
						
						public void actionPerformed(ActionEvent e){
							if(Board.getGrid()[x][y] == 1){
								Board.SecPassRm1To5(Board.getCurrentPlayer());
								exampleMethod();
							}
							if(Board.getGrid()[x][y] == 5){
								Board.SecPassRm5To1(Board.getCurrentPlayer());
								exampleMethod();
							}
							if(Board.getGrid()[x][y] == 3){
								Board.SecPassRm3To8(Board.getCurrentPlayer());
								exampleMethod();
							}
							if(Board.getGrid()[x][y] == 8){
								Board.SecPassRm8To3(Board.getCurrentPlayer());
								exampleMethod();
							}
						}
					});
				}
			}
		});

	}
	public void exampleMethod() {
		panel2.removeAll();

		int[][] grid = Board.getGrid();
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[i].length; j++) {
				int num = grid[i][j];

				JButton b = new JButton();
				// b.setPreferredSize(new Dimension(10, 10));
				b.setOpaque(true);
				// SECRET PASSAGE
				if (num == 60 || num == 50) {
					b.setBackground(Color.BLACK);
				}
				// WALLS
				if (num == 11) {
					b.setBackground(Color.GRAY);
				}
				// DOORS
				if (num == 22 || num == 23 || num == 24 || num == 25 || num == 26 || num == 27 
						|| num == 28 || num == 29 || num == 30) {
					b.setForeground(Color.PINK);
					b.setBackground(Color.PINK);
				}
				// ROOMS
				if (num == 1) {
					b.setBackground(Color.RED);
				}

				if (num == 2) {
					b.setBackground(Color.BLUE);
				}
				if (num == 3) {
					b.setBackground(Color.GREEN);
				}
				if (num == 4) {
					b.setBackground(Color.YELLOW);
				}
				if (num == 5) {
					b.setBackground(Color.CYAN);
				}
				if (num == 6) {
					b.setBackground(Color.ORANGE);
				}
				if (num == 7) {
					b.setBackground(Color.MAGENTA);
				}
				if (num == 8) {
					b.setBackground(new Color(12, 167, 23));
				}
				if (num == 9) {
					b.setBackground(new Color(54, 100, 98));
				}
				if (num == 10) {
					b.setBackground(new Color(215, 145, 20));
				}
				// Players
				if (num == 12) {
					b.setBackground(new Color(234, 40, 100));
					b.setText("MS");
				}
				if (num == 13) {
					b.setBackground(new Color(15, 60, 185));
					b.setText("PP");
				}
				if (num == 14) {
					b.setBackground(new Color(40, 209, 12));
					b.setText("MG");
				}
				if (num == 15) {
					b.setBackground(new Color(255, 255, 255));
					b.setText("MW");
				}
				if (num == 16) {
					b.setBackground(new Color(210, 10, 150));
					b.setText("MP");
				}
				if (num == 17) {
					b.setBackground(new Color(60, 110, 98));
					b.setText("CM");
				}
				panel2.add(b);
			}
		}
		frame.repaint();
		frame.pack();
	}

}
