package edu.buffalo.cse116;

public class Dice {
    
    
    public int die;   // Number showing on the die.
    
    public Dice() {
            // Constructor.  Rolls the dice, so that they initially
            // show some random values.
        roll();  // Call the roll() method to roll the dice.
    }
    
    public int roll() {
            // Roll the dice by setting each of the dice to be
          // a random number between 1 and 6.
          die = (int)(Math.random()*6) + 1;
   return die;    
   
    }
    
    public int getDieRoll(){
    
   return die;
    }
    
    
    
} // end class dice