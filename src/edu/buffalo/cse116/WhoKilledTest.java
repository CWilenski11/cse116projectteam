package edu.buffalo.cse116;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import edu.buffalo.cse116.Cards;
import edu.buffalo.cse116.Player;

public class WhoKilledTest {

	@Test
	public void HorizontalMovetest() {

	}

	// test cases 8 out of the 17 -kevin
	@Test

	// test that a horizontal move to the right is legal
	public void testHorizontalRightLegal() {
		Cards c = new Cards();
		Dice d = new Dice();
		c.CreateDeck();
		Player h = new Player(c.generateHand());
		h.setLocation(3, 3);
		int f = d.roll();
		h.MoveRight(f);
		assertEquals(h.getxLocation(), 3+f);

	}

	@Test
	public void testHorizontalLeftLegal() {
		Cards c = new Cards();
		Dice d = new Dice();
		c.CreateDeck();
		Player h = new Player(c.generateHand());
		h.setLocation(10, 2);
		int f = d.roll();
		h.MoveLeft(f);

		assertEquals(h.getxLocation(), 10 - f);

	}
	@Test
	public void testVerticalalUpLegal() {
		Cards c = new Cards();
		Dice d = new Dice();
		c.CreateDeck();
		Player h = new Player(c.generateHand());
		h.setLocation(10, 2);
		int f = d.roll();
		h.MoveUp(f);

		assertEquals(h.getyLocation(), 2 -f);
	}
	
	@Test
	public void testVerticalalDownLegal() {
		Cards c = new Cards();
		Dice d = new Dice();
		c.CreateDeck();
		Player h = new Player(c.generateHand());
		h.setLocation(10, 2);
		int f = d.roll();
		h.MoveDown(f);

		assertEquals(h.getyLocation(), 2+f );
	}
	
	@Test 
	public void testHorizontalVerticalLegal(){
		Cards c = new Cards();
		Dice d = new Dice();
		c.CreateDeck();
		Player h = new Player(c.generateHand());
		h.setLocation(10, 2);
		int f = d.roll();
		h.MoveDown(f);
		h.MoveLeft(f);

		assertEquals(h.getxLocation(),10-f);
		assertEquals(h.getyLocation(),2+f);
		
	}
	@Test
	
	public void SpotIsFull(){
		Cards c = new Cards();
		c.CreateDeck();
		Player h = new Player(c.generateHand());
		h.setLocation(10, 2);
		
		assertTrue(h.spotFull(10,2));		
		
	}
	@Test
	public void SpotIsNotFull(){
		Cards c = new Cards();
		c.CreateDeck();
		Player h = new Player(c.generateHand());
		h.setLocation(10, 2);
		
		assertFalse(h.spotFull(10,3));
	}

	
	
	@Test
	 public void illegalDiagonalMove(){
		// empty because our code doesn't permit this. (professor hertz told me to leave it blank as well)
	}
	
	@Test

	public void testDieLimit() {
		Dice d = new Dice();
		assertTrue(d.roll() < 7);
	}

	@Test
	public void diceroll() {
		Dice d = new Dice();

		assertTrue(d.roll() <= 6 && d.roll() >= 1);
	}
	
	@Test
	public void onTheDoor(){
		Board b = new Board();
		Player h = new Player();
		h.setLocation(3, 4);
		assertTrue(b.IsDoor(3,4));	
			
		}
	@Test
	public void secretPassageTest(){
		Board b = new Board();
		Player h = new Player();
		h.setLocation(22, 3);
		assertTrue(b.CanUseSecretPassage(22, 3));
	}
	
	@Test
	public void playerInWallTest(){
	Board b = new Board();
	Player h = new Player();
	h.setLocation(4, 2);
	assertFalse(b.inWall(4, 2));
	}
		
	}


