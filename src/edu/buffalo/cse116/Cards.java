package edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.Collections;

public class Cards {
	String _type;
	Cards _card;
	
    ArrayList<Cards> _deck;
    ArrayList<Cards> _weapons;
	private ArrayList<Cards> _rooms;
	private ArrayList<Cards> _characters;
	ArrayList<Cards>  _envelope;
	
	public Cards(String t) {
		 _type=t;

	}

	public Cards() {
		_deck = new ArrayList<Cards>();
		_weapons = new ArrayList<Cards>();
		_rooms = new ArrayList<Cards>();
		_characters = new ArrayList<Cards>();
		_envelope = new ArrayList<Cards>();
		_card= new Cards("");
	}

	// Create an individual card
	public Cards createCard(String _type) {
		Cards c = new Cards(_type);
		return c;
	}
    
	public String getType(){
		return _type;
	}
	// CreateCard("Weapon") creates a card with name Weapon
	// CreateCard("Room") creates a card with name room
	public ArrayList<Cards> WeaponCards() {

		// Creates a Card (knife) etc. and add it to Array list all in one call
		// 6
		_weapons.add(createCard("CandleStick"));
		_weapons.add(createCard("Dagger"));
		_weapons.add(createCard("Lead Pipe"));
		_weapons.add(createCard("Revolver"));
		_weapons.add(createCard("Rope"));
		_weapons.add(createCard("Spanner"));

		return _weapons;
	}

	public ArrayList<Cards> RoomCards() {
		

		// Creates a Card (kitchen) etc. and add it to Array List all in one
		// call
		// 9 cards
		_rooms.add(_card.createCard("Kitchen"));
		_rooms.add(_card.createCard("Ballroom"));
		_rooms.add(_card.createCard("Dining Room"));
		_rooms.add(_card.createCard("Lounge"));
		// I added more rooms so we have a total of 9 - kevin
		_rooms.add(_card.createCard("Game Room"));
		_rooms.add(_card.createCard("Library"));
		_rooms.add(_card.createCard("Indoor Pool"));
		_rooms.add(_card.createCard("Bedroom"));
		_rooms.add(_card.createCard("Guest Room"));

		return _rooms;

	}

	public ArrayList<Cards> CharacterCards() {
		

		// 6 cards
		_characters.add(_card.createCard("Miss Scarlet"));
		_characters.add(_card.createCard("Professor Plum"));
		_characters.add(_card.createCard("Mr. Green"));
		_characters.add(_card.createCard("Mrs. White"));
		_characters.add(_card.createCard("Mrs. Peacock"));
		_characters.add(_card.createCard("Col. Mustard"));

		return _characters;
	}

	public ArrayList<Cards> CreateDeck() {
		Envelopecards();
		for (Cards c : _weapons) {

			_deck.add(c);
		}
		for (Cards c : _rooms) {
			_deck.add(c);
		}
		for (Cards c : _characters) {
			_deck.add(c);
		}
		return _deck;
	}

	public ArrayList<Cards> generateHand() {
		// adds 6 cards to a players hand

		ArrayList<Cards> Gh = new ArrayList<Cards>();
		for (int i = 0; i <3; i++) {

			Gh.add(_deck.remove(i));
		}

		return Gh;
	}
	
	public ArrayList<Cards> removeFinalThree(){
		ArrayList<Cards> Gh = new ArrayList<Cards>();
		Gh.add(_deck.remove(0));
		Gh.add(_deck.remove(0));
		Gh.add(_deck.remove(0));
		return Gh;
	}
		
	
	public ArrayList<String> generateHandString(ArrayList<Cards> a){
		ArrayList<String> s = new ArrayList<String>();
		for(Cards c: a){
			s.add(c.getType());
		}
		return s;
	}
	
	

	// method that adds a card from weapons,rooms,character to an envelope and
	// returns the envelope
	public ArrayList<Cards> Envelopecards() {
		WeaponCards();
		RoomCards();
		CharacterCards();
		

		_envelope.add(_weapons.remove(0));
		_envelope.add(_rooms.remove(0));
		_envelope.add(_characters.remove(0));

		return _envelope;

	}
}
