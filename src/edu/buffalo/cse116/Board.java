package edu.buffalo.cse116;

import java.util.ArrayList;

public class Board {
	 private int[][] Board;
	 Player a;
	 Player b;
	 Player d;
	 Player e;
	 Player f;
	 Player g;
	 ArrayList<Player> Playerlist= new  ArrayList<Player> ();
	 GameGUI _gui;
	//private int _state;
	// private int _whoseTurn;
	private ArrayList<Cards> deck;

	public Board() {
		
		
		
		Board = new int[25][25];

		for (int i = 0; i <= 24; i++) {

			for (int j = 0; j <= 24; j++) {
				Board[i][j] = 0;
			}
		}
		// Room 1 5x5
		for (int i = 0; i < 5; i++) {

			for (int j = 0; j < 5; j++) {
				Board[i][j] = 1;
			}
		}
		// Room 9 5x5
		for (int i = 0; i < 5; i++) {

			for (int j = 9; j < 14; j++) {
				Board[i][j] = 9;
			}
		}
		// room 8 but its 5x4
		for (int i = 0; i < 4; i++) {

			for (int j = 20; j < 25; j++) {
				Board[i][j] = 8;
			}
		}
		// room 2 5x5
		for (int i = 9; i < 14; i++) {

			for (int j = 0; j < 5; j++) {
				Board[i][j] = 2;
			}
		}
		// room 3 5x5
		for (int i = 20; i < 25; i++) {

			for (int j = 0; j < 5; j++) {
				Board[i][j] = 3;
			}
		}
		// room 4 5x5
		for (int i = 20; i < 25; i++) {

			for (int j = 9; j < 14; j++) {
				Board[i][j] = 4;
			}
		}

		// room 5 5x5

		for (int i = 21; i < 25; i++) {

			for (int j = 20; j < 25; j++) {
				Board[i][j] = 5;
			}
		}
		// room 6 5x4
		for (int i = 14; i < 18; i++) {

			for (int j = 20; j < 25; j++) {
				Board[i][j] = 6;
			}
		}
		// room 7
		for (int i = 7; i < 11; i++) {

			for (int j = 20; j < 25; j++) {
				Board[i][j] = 7;
			}
		}

		// envelope
		for (int i = 9; i < 14; i++) {

			for (int j = 9; j < 14; j++) {
				Board[i][j] = 10;
			}
		}

		// All entrances to the rooms are numbered 22
		// entrance for room 1
		for (int i = 3; i < 4; i++) {
			for (int j = 4; j < 5; j++) {
				Board[i][j] = 22;
			}
		}
		// entrance for room 2
		for (int i = 11; i < 12; i++) {
			for (int j = 4; j < 5; j++) {
				Board[i][j] = 23;
			}
		}
		// entrance for room 3
		for (int i = 20; i < 21; i++) {
			for (int j = 2; j < 3; j++) {
				Board[i][j] = 24;
			}
		}
		// entrance for room 4
		for (int i = 21; i < 22; i++) {
			for (int j = 13; j < 14; j++) {
				Board[i][j] = 25;
			}
		}
		// entrance for room 5
		for (int i = 21; i < 22; i++) {
			for (int j = 23; j < 24; j++) {
				Board[i][j] = 26;
			}
		}
		// entrance for room 6
		for (int i = 16; i < 17; i++) {
			for (int j = 20; j < 21; j++) {
				Board[i][j] = 27;
			}
		}
		// entrance for room 7
		for (int i = 8; i < 9; i++) {
			for (int j = 20; j < 21; j++) {
				Board[i][j] = 28;
			}
		}
		// entrance for room 8
		for (int i = 3; i < 4; i++) {
			for (int j = 21; j < 22; j++) {
				Board[i][j] = 29;
			}
		}
		// entrance for room 9
		for (int i = 4; i < 5; i++) {
			for (int j = 11; j < 12; j++) {
				Board[i][j] = 30;
			}
		}

		// There is a secret Passage from room 1 to room 5 numbered 50
		// Secret Passage for room 1
		for (int i = 0; i < 1; i++) {
			for (int j = 0; j < 1; j++) {
				Board[i][j] = 50;
			}
		}
		// Secret Passage for room 5
		for (int i = 24; i < 25; i++) {
			for (int j = 24; j < 25; j++) {
				Board[i][j] = 50;
			}
		}
		// There is a secret passage from room 3 to room 8 numbered 60
		// Secret Passage for room 3
		for (int i = 24; i < 25; i++) {
			for (int j = 0; j < 1; j++) {
				Board[i][j] = 60;
			}
		}
		// Secret Passage for room 8
		for (int i = 0; i < 1; i++) {
			for (int j = 24; j < 25; j++) {
				Board[i][j] = 60;
			}
		}
		// Wall for room 1
				for (int i = 4; i <5; i++) {
					for (int j = 0; j < 5; j++) {
						Board[i][j] = 11;
					}
				}
				// Wall for room 1
				for (int i = 0; i <3; i++) {
					for (int j = 4; j < 5; j++) {
						Board[i][j] = 11;
					}
				}
		// Wall for room 2
				for (int i = 13; i <14; i++) {
					for (int j = 0; j < 5; j++) {
						Board[i][j] = 11;
					}
				}
				// Wall for room 2
				for (int i = 9; i <10; i++) {
					for (int j = 0; j < 5; j++) {
						Board[i][j] = 11;
					}
				}
				// Wall for room 2
				for (int i = 10; i <11; i++) {
					for (int j = 4; j < 5; j++) {
						Board[i][j] = 11;
					}
				}
				// Wall for room 2
				for (int i =12 ; i <13; i++) {
					for (int j = 4; j < 5; j++) {
						Board[i][j] = 11;
					}
				}
		// Wall for room 3
				for (int i = 20; i <25; i++) {
					for (int j = 4; j < 5; j++) {
						Board[i][j] = 11;
					}
				}
				// Wall for room 3
				for (int i = 20; i <21; i++) {
					for (int j = 0; j < 2; j++) {
						Board[i][j] = 11;
					}
				}
				// Wall for room 3
				for (int i = 20; i <21; i++) {
					for (int j = 3; j < 4; j++) {
						Board[i][j] = 11;
					}
				}
				//Wall for room 4*
				for(int i = 20; i < 21; i++){
					for(int j = 9; j < 14; j++){
						Board[i][j] = 11;
					}
				}
				// Wall room 4
				for(int i = 20; i < 25; i++){
					Board[i][9]= 11;
				}
				for(int i = 22; i < 25; i++){
					Board[i][13] = 11;
				}
				//Wall room 5
				for(int i = 21; i < 25; i++){
					Board[i][20] = 11;
				}
				for(int j = 21; j < 23; j++){
					Board[21][j] = 11;
				}
				Board[21][24] = 11;
				//Wall room 6
				for(int j = 20; j < 25; j++){
					Board[14][j] = 11;
				}
				for(int j = 20; j< 25; j++){
					Board[17][j] = 11;
				}
				Board[15][20] = 11;
				//Wall room 7
				for (int j = 20; j < 25; j++){
					Board[7][j] = 11;
				}
				for (int j = 20; j < 25; j++){
					Board[10][j] = 11;
				}
				Board[9][20] = 11;
				// Wall room 8
				for (int i = 0; i < 4; i++){
					Board[i][20] = 11;
				}
				for (int j = 22; j < 25; j++){
					Board[3][j] = 11;
				}
				// Wall room 9
				for (int i = 0; i < 5; i++){
					Board[i][9] = 11;
				}
				for (int i = 0; i < 5; i++){
					Board[i][13] = 11;
				}
				Board[4][10] = 11;
				Board[4][12] = 11;
				createAllPlayers();
				Board[a.getxLocation()][a.getyLocation()] = 12;
				Board[b.getxLocation()][b.getyLocation()] = 13;
				Board[d.getxLocation()][d.getyLocation()] = 14;
				Board[e.getxLocation()][e.getyLocation()] = 15;
				Board[f.getxLocation()][f.getyLocation()] = 16;
				Board[g.getxLocation()][g.getyLocation()] = 17;
				//_whoseTurn=0;
				//_state=0;
				
		//		state=0;
				//_gui.update();
			
			//	_gui.update();
				
				
	}
	
	//public  Player getCurrentPlayer(){
		//return _players.get(_whoseTurn);
	//}
	
	//public int getState(){
	//	return _state;
	//	}
	
	public void setGUI(GameGUI g) {
		_gui = g;
	}
		
	public boolean IsDoor(int x, int y){
		Board b = new Board();
		Player h = new Player();
		h.setLocation(x, y);
		int i = h.getxLocation();
		int j = h.getyLocation();
		if(Board[i][j] == 22 || Board[i][j] == 23 || Board[i][j] == 24 || Board[i][j] == 25 || 
				Board[i][j] == 26 || Board[i][j] == 27 || Board[i][j] == 28 ||
				Board[i][j] == 29 || Board[i][j] == 30 ){
			return true;
		}
		return false;
		}
	

	public boolean NotValidMove(int x, int y) {
		// To check if the move of the player is valid or not
		// The player cannot move on the board with numbers 1-10(rooms)
		if (Board[x][y] == 1 || Board[x][y] == 2 || Board[x][y] == 3 || Board[x][y] == 4 || Board[x][y] == 5
				|| Board[x][y] == 6 || Board[x][y] == 7 || Board[x][y] == 8 || Board[x][y] == 9 || Board[x][y] == 10
				|| Board[x][y] == 50 || Board[x][y] == 60) {
			return true;

		}
		return true;
	}

	public boolean ValidMove(int x, int y) {
		// To make sure the player moves through the hallway and the door
		if (Board[x][y] == 0 || Board[x][y] == 22 || Board[x][y] == 23 || Board[x][y] == 24 || Board[x][y] == 25 || 
				Board[x][y] == 26 || Board[x][y] == 27 || Board[x][y] == 28 ||
				Board[x][y] == 29 || Board[x][y] == 30) {
			return true;
		}
		return false;
	}
	
	public boolean CanUseSecretPassage(int x, int y){
		Board b = new Board();
		Player h = new Player();
		h.setLocation(x, y);
		int i = h.getxLocation();
		int j = h.getyLocation();
		if(Board[i][j]==1 ||Board[i][j]==3 || Board[i][j]==5
				||Board[i][j]==8){
			return true;
		}
		
		return false;
	}
	
	public boolean inWall(int x, int y){
		Board b = new Board();
		Player h = new Player();
		h.setLocation(x, y);
		int i = h.getxLocation();
		int j = h.getyLocation();
		if(Board[i][j]==11){
			return false;
		}
		return true;
	}


	public void printBoard() {
		for (int i = 0; i < Board.length; i++) {
			for (int j = 0; j < Board[i].length; j++) {

				System.out.print("[" + Board[i][j] + "]" + " ");

			}
			System.out.print("\n");
		}

	}
	public int[][] getGrid(){
		return Board;
	}
	public ArrayList<Player> createAllPlayers(){
	
		Cards c = new Cards();
		
		a = new Player();
		 b= new Player();
		 d = new Player();
		 e= new Player();
		 f = new Player();
		 g= new Player();
		String a1= new String ("Miss Scarlet");
		String b1= new String ("Professor Plum");
		String d1= new String ("Mr.Green");
		String e1= new String ("Mrs.White");
		String f1= new String ("Mrs.Peacock");
		String g1= new String ("Col.Mustard");
		
		a.setName(a1);
		a.setLocation(0,6);
		b.setName(b1);
		b.setLocation(0,17);
		d.setName(d1);
		d.setLocation(24,6 );
		e.setName(e1);
		e.setLocation(24, 17);
		f.setName(f1);
		f.setLocation(6,0);
		g.setName(g1);
		g.setLocation(16, 0);
		
		Playerlist.add(a);
		Playerlist.add(b);
		Playerlist.add(d);
		Playerlist.add(e);
		Playerlist.add(f);
		Playerlist.add(g);
		
		return Playerlist;
		  
		}
	
	public void SwitchPlayers(){
		Playerlist.add(Playerlist.remove(0));
	}
	
	public Player getCurrentPlayer() {
		return Playerlist.get(0);
	}
public void MoveRight(Player h) {
	if(ValidMove(h.getxLocation(),h.getyLocation()+1)){
		if(h == a){
			Board[h.getxLocation()][h.getyLocation()+1] = 12;
			Board[h.getxLocation()][h.getyLocation()] = 0;
			h.setLocation(h.getxLocation(),h.getyLocation()+1);
			
		}
		if(h == b){
			Board[h.getxLocation()][h.getyLocation()+1] = 13;
			Board[h.getxLocation()][h.getyLocation()] = 0;
			h.setLocation(h.getxLocation(),h.getyLocation()+1);
		}
		if(h == d){
			Board[h.getxLocation()][h.getyLocation()+1] = 14;
			Board[h.getxLocation()][h.getyLocation()] = 0;
			h.setLocation(h.getxLocation(),h.getyLocation()+1);
		}
		if(h == e){
			Board[h.getxLocation()][h.getyLocation()+1] = 15;
			Board[h.getxLocation()][h.getyLocation()] = 0;
			h.setLocation(h.getxLocation(),h.getyLocation()+1);
		}
		if(h == f){
			Board[h.getxLocation()][h.getyLocation()+1] = 16;
			Board[h.getxLocation()][h.getyLocation()] = 0;
			h.setLocation(h.getxLocation(),h.getyLocation()+1);
		}
		if(h == g){
			Board[h.getxLocation()][h.getyLocation()+1] = 17;
			Board[h.getxLocation()][h.getyLocation()] = 0;
			h.setLocation(h.getxLocation(),h.getyLocation()+1);
		}
	}
	else{
		System.out.println("Illegal move");
	}
}

public void MoveLeft(Player h) {
	if(ValidMove(h.getxLocation(),h.getyLocation()-1)){
	if(h == a){
		Board[h.getxLocation()][h.getyLocation()-1] = 12;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation(),h.getyLocation()-1);
	}
	if(h == b){
		Board[h.getxLocation()][h.getyLocation()-1] = 13;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation(),h.getyLocation()-1);
	}
	if(h == d){
		Board[h.getxLocation()][h.getyLocation()-1] = 14;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation(),h.getyLocation()-1);
	}
	if(h == e){
		Board[h.getxLocation()][h.getyLocation()-1] = 15;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation(),h.getyLocation()-1);
	}
	if(h == f){
		Board[h.getxLocation()][h.getyLocation()-1] = 16;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation(),h.getyLocation()-1);
	}
	if(h == g){
		Board[h.getxLocation()][h.getyLocation()-1] = 17;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation(),h.getyLocation()-1);
	}
}
	else{
		System.out.println("Illegal move");
	}
}

public void MoveDown(Player h) {
	if(ValidMove(h.getxLocation()+1,h.getyLocation())){
	if(h == a){
		Board[h.getxLocation()+1][h.getyLocation()] = 12;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation()+1,h.getyLocation());
	}
	if(h == b){
		Board[h.getxLocation()+1][h.getyLocation()] = 13;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation()+1,h.getyLocation());
	}
	if(h == d){
		Board[h.getxLocation()+1][h.getyLocation()] = 14;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation()+1,h.getyLocation());
	}
	if(h == e){
		Board[h.getxLocation()+1][h.getyLocation()] = 15;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation()+1,h.getyLocation());
	}
	if(h == f){
		Board[h.getxLocation()+1][h.getyLocation()] = 16;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation()+1,h.getyLocation());
	}
	if(h == g){
		Board[h.getxLocation()+1][h.getyLocation()] = 17;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation()+1,h.getyLocation());
	}
}
else{
		System.out.println("Illegal move");
	}
}

public void MoveUp(Player h) {
	if(ValidMove(h.getxLocation()-1,h.getyLocation())){
	if(h == a){
		Board[h.getxLocation()-1][h.getyLocation()] = 12;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation()-1,h.getyLocation());
	}
	if(h == b){
		Board[h.getxLocation()-1][h.getyLocation()] = 13;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation()-1,h.getyLocation());
	}
	if(h == d){
		Board[h.getxLocation()-1][h.getyLocation()] = 14;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation()-1,h.getyLocation());
	}
	if(h == e){
		Board[h.getxLocation()-1][h.getyLocation()] = 15;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation()-1,h.getyLocation());
	}
	if(h == f){
		Board[h.getxLocation()-1][h.getyLocation()] = 16;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation()-1,h.getyLocation());
	}
	if(h == g){
		Board[h.getxLocation()-1][h.getyLocation()] = 17;
		Board[h.getxLocation()][h.getyLocation()] = 0;
		h.setLocation(h.getxLocation()-1,h.getyLocation());
	}
}
	else{
		System.out.println("Illegal move");
	}

}

public void SecPassRm1To5(Player h){
	if(h == a){
	Board[21][23] = 12;
	Board[h.getxLocation()][h.getyLocation()] = 22;
	h.setLocation(21, 23);
	}
	if(h == b){
		Board[21][23] = 13;
		Board[h.getxLocation()][h.getyLocation()] = 22;
		h.setLocation(21, 23);
	}
	if(h == d){
		Board[21][23] = 14;
		Board[h.getxLocation()][h.getyLocation()] = 22;
		h.setLocation(21, 23);
	}
	if(h == e){
		Board[21][23] = 15;
		Board[h.getxLocation()][h.getyLocation()] = 22;
		h.setLocation(21, 23);
	}
	if(h == f){
		Board[21][23] = 16;
		Board[h.getxLocation()][h.getyLocation()] = 22;
		h.setLocation(21, 23);
	}
	if(h == g){
		Board[21][23] = 17;
		Board[h.getxLocation()][h.getyLocation()] = 22;
		h.setLocation(21, 23);
	}
}
public void SecPassRm5To1(Player h){
	if(h == a){
	Board[3][4] = 12;
	Board[h.getxLocation()][h.getyLocation()] = 26;
	h.setLocation(3, 4);
	}
	if(h == b){
		Board[3][4] = 13;
		Board[h.getxLocation()][h.getyLocation()] = 26;
		h.setLocation(3, 4);
	}
	if(h == d){
		Board[3][4] = 14;
		Board[h.getxLocation()][h.getyLocation()] = 26;
		h.setLocation(3, 4);
	}
	if(h == e){
		Board[3][4] = 15;
		Board[h.getxLocation()][h.getyLocation()] = 26;
		h.setLocation(3, 4);
	}
	if(h == f){
		Board[3][4] = 16;
		Board[h.getxLocation()][h.getyLocation()] = 26;
		h.setLocation(3, 4);
	}
	if(h == g){
		Board[3][4] = 17;
		Board[h.getxLocation()][h.getyLocation()] = 26;
		h.setLocation(3, 4);
	}
}
public void SecPassRm3To8(Player h){
	if(h == a){
	Board[3][21] = 12;
	Board[h.getxLocation()][h.getyLocation()] = 24;
	h.setLocation(3, 21);
	}
	if(h == b){
		Board[3][21] = 13;
		Board[h.getxLocation()][h.getyLocation()] = 24;
		h.setLocation(3, 21);
	}
	if(h == d){
		Board[3][21] = 14;
		Board[h.getxLocation()][h.getyLocation()] = 24;
		h.setLocation(3, 21);
	}
	if(h == e){
		Board[3][21] = 15;
		Board[h.getxLocation()][h.getyLocation()] = 24;
		h.setLocation(3, 21);
	}
	if(h == f){
		Board[3][21] = 16;
		Board[h.getxLocation()][h.getyLocation()] = 24;
		h.setLocation(3, 21);
	}
	if(h == g){
		Board[3][21] = 17;
		Board[h.getxLocation()][h.getyLocation()] = 24;
		h.setLocation(3, 21);
	}
}

public void SecPassRm8To3(Player h){
	if(h == a){
	Board[20][2] = 12;
	Board[h.getxLocation()][h.getyLocation()] = 29;
	h.setLocation(20, 2);
	}
	if(h == b){
		Board[20][2] = 13;
		Board[h.getxLocation()][h.getyLocation()] = 29;
		h.setLocation(20, 2);
	}
	if(h == d){
		Board[20][2] = 14;
		Board[h.getxLocation()][h.getyLocation()] = 29;
		h.setLocation(20, 2);
	}
	if(h == e){
		Board[20][2] = 15;
		Board[h.getxLocation()][h.getyLocation()] = 29;
		h.setLocation(20, 2);
	}
	if(h == f){
		Board[20][2] = 16;
		Board[h.getxLocation()][h.getyLocation()] = 29;
		h.setLocation(20, 2);
	}
	if(h == g){
		Board[20][2] = 17;
		Board[h.getxLocation()][h.getyLocation()] = 29;
		h.setLocation(20, 2);
	}
}	
	

	
	/*public ArrayList<Cards> ShowCards(){
		
		return
		
		
	}*/
}
