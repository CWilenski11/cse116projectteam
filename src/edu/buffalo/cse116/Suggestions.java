package edu.buffalo.cse116;

import java.util.ArrayList;


public class Suggestions {
	private ArrayList<Player> Playerlist= new  ArrayList<Player> ();
	private Board board;
	public Suggestions(){
		Cards c= new Cards();
		c.CreateDeck();
		Player p = new Player(c.generateHand());
	}
	
	/*	 public String getRoom(){
		 	if(Board[i][j] == 22){
		 	return 
		 	}
		 	if(Board[i][j] == 23){
		 	return
		 	}
		 	if(Board[i][j] == 24){
		 	return 
		 	if(Board[i][j] == 25){
		 	return 
		 	if(Board[i][j] == 26){
		 	return 
		 	if(Board[i][j] == 27){
		 	return 	
		 	if(Board[i][j] == 28){
		 	return 
		 	if(Board[i][j] == 29){
		 	return 		 
		 	if(Board[i][j] == 30){
		 	return 		 				 		 			 	
		 }		
		 	*/
		 
		    // method that runs when isDoor returns true. Creates a suggestion Arraylist that takes a room,weapon suspect suggestion
		    // and collects the all the other players "Hands"(arraylist that contains their cards at hand) and runs makeSuggestion() method.
		 public ArrayList<Cards> enterDoor(Player p){
			 ArrayList<Cards> suggestions= new ArrayList<Cards>();
					
			 
			 if( board.IsDoor( p.getxLocation(), p.getyLocation()) == true){	
		 	//1st	// suggestions.add(player.getRoom); // need getRoom method. 
			//2nd	// suggestions.add( weapon ); // we will use a Jframe popup that will make you pick the other 2 suggestions
		 											// list every single card
		 	//3rd	// suggestions.add( suspect );
		    
					}
					return suggestions;
				 	
		 }
		 //creates arraylist of players
		 public ArrayList<Player> getOtherPlayerHands(){
		 		ArrayList<Player> allPlayerHands= new ArrayList<Player>();
		 		for( Player p: Playerlist)
		 		allPlayerHands.add(p);

		 		 return allPlayerHands;
		 	}
		
		 
		 
		 	//once suggestions and Arraylist of Arraylists have been made, run method makeSuggestion
		 public void makeSuggestion(ArrayList<Cards> suggestions,ArrayList<Player> allPlayerHands ){
			 compareToSuggestion( suggestions, allPlayerHands);	
		 }

		
	
		 
		 public Player compareToSuggestion(ArrayList<Cards> suggestionList, ArrayList<Player> compareLists){

				  //For each card in first list
				  for(Cards first: suggestionList){

				      //For each list you wish to compare against
				      for(Player nextPlayer: compareLists){

				          //For each card in the list compare against first list
				          for(Cards next: nextPlayer.getHand()){
				              if(first.equals(next)){
				          return nextPlayer;
				          }
				              }
				          }
				      }
				      
				return null;
				
				  }
			
		}